const globby = require('globby');

const PLACES = {
  'next': {
    dir: '.next/server/pages',
    searchedFiles: '.js',
    ignoreElements: [
      '.next/**/_app.js',
      '.next/**/_error.js',
      '.next/**/_document.js',
      '.next/**/index.js',
      '.next/**/[pid].js'
    ]
  },
  'gatsby': {
    dir: 'public',
    searchedFiles: '.html',
    ignoreElements: [
      '*/index.html',
      '*/404/index.html',
    ]
  }
}

exports.default = async function (
  baseUrl = 'http://localhost:3000',
  type = 'gatsby',
  options = {},
) {
  const returnedUrls = [
    `${baseUrl}/`,
  ];
  const mergedOptions = {
    ...PLACES[type],
    ...options,
  }

  const {
    dir,
    searchedFiles,
    ignoreElements,
  } = mergedOptions;


  if (PLACES[type] !== undefined) {
    const filesArray = await globby.sync([
      `${dir}/**/*${searchedFiles}`,
    ], {
      ignore: ignoreElements
    }).map(file => {
      const trimmedUrl = file.substring(
        dir.length,
        file.includes(`/index${searchedFiles}`) ?
          file.lastIndexOf(`/index${searchedFiles}`) :
          file.lastIndexOf('.')
      );

      return `${baseUrl}${trimmedUrl}`
    }).filter((currentValue, index, array) => array.indexOf(currentValue) === index)

    returnedUrls.push(...filesArray);
  }

  return returnedUrls;
};
