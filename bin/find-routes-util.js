#!/usr/bin/env node
'use strict';

process.on('unhandledRejection', err => {
  throw err;
});

const fs = require('fs-extra');

// TODO: Add support for args from CLI
const args = process.argv.slice(2);
const exportedMethod = require('../index');

new Promise((resolve, reject) => {
  try {
    console.log('Draining your routes...');
    return resolve(exportedMethod.default());
  } catch (error) {
    reject(`Something went wrong: ${error}`);
  }

  return;
}).then(value => {
  const formattedValues = value.map(item => `--collect.url=${item}`).join(' ')

  try {
    fs.writeFileSync('cli-arguments.txt', formattedValues)

    console.log()
    console.log()
    console.log('Draining stopped...')
    console.log()
    console.log()
    console.log('File saved on the server!');
  } catch (error) {
    console.log()
    console.log(`Error with saving file: ${error}`);
  }
}).catch(error => {
  console.log()
  console.log(`Something went wrong: ${error}`);
})
